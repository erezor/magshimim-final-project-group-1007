import React from "react";
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Login from './login.component';
import Register from './register.component';
import RegisterVerify from './register_verify.component';
import ScanInfo from "./scan_info.component";
import Scans from "./scans.component";
import Error404 from "./error404.component";
import {useAuth} from "./auth";
import AboutUs from "./aboutus.component"
const PrivateRoute = ({ component: Component, ...rest }) => {
  const [logged] = useAuth();

  return <Route {...rest} render={(props) => (
    logged
      ? <Component {...props} />
      : <Redirect to='/login' />
  )} />
}
function App() {
  return (
    <Router>
      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route exact path="/register-verify">
          <RegisterVerify />
        </Route>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/register">
          <Register />
        </Route>
        <Route exact path="/about-us">
          <AboutUs/>
        </Route>
        <PrivateRoute exact path="/scan-info" component={ScanInfo} />
        <PrivateRoute exact path="/dashboard" component={Scans} />
        <Route exact path="/">
          <Login />
        </Route>
        <Route>
          <Error404/>
        </Route>
          
      </Switch>
    </Router>
  );
}

export default App;
