import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import AddIcon from '@material-ui/icons/Add';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import { authFetch } from './auth';
const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    listItem: {
        padding: theme.spacing(1, 0),
    },
    total: {
        fontWeight: 700,
    },
    title: {
        marginTop: theme.spacing(2),
    },
    marginAutoContainer: {
        width: 500,
        height: 80,
        display: 'flex',
    },
    marginAutoItem: {
        margin: 'auto'
    },
    alignItemsAndJustifyContent: {
        width: 500,
        height: 80,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
}));





export default function NewScanDialog() {
    const [open, setOpen] = useState(false);
    const [configOpen, setConfigOpen] = useState(false);
    const [includeLogin, setincludeLogin] = useState(false);
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [website_url, setWebsiteUrl] = useState('')
    const [passwordParam, setPasswordParam] = useState('');
    const [usernameParam, setUsernameParam] = useState('');
    const [postLogin, setPostLogin] = useState('');
    const [error_message, setErrorMessage] = useState('');
    const [vertical] = useState('top');
    const [horizontal] = useState('center');
    const [hidden, setHidden] = useState(false);

    const [config, setConfig] = useState({
        sql: true,
        xss: true,
        os_i: true,
        php: true,
        iframe: true,
        html: true,
        idor: true,
    })

    const classes = useStyles();

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    const handleClickConfigOpen = () => {
        setConfigOpen(true);
    };

    const handleConfigChange = (e, val) => {
        const newVal = !config[val]
        setConfig({
            ...config, [val]: newVal,
        }
        );
    }

    const handleConfigClose = () => {
        setConfig({
            sql: true,
            xss: true,
            os_i: true,
            php: true,
            iframe: true,
            html: true,
            idor: true,
        });
        setOpen(false);
        setConfigOpen(false);
    };

    const handleChecked = (e) => {
        setincludeLogin(!includeLogin);
    }

    const handleUsernameChange = (e) => {
        setUsername(e.target.value)
    }

    const handlePasswordChange = (e) => {
        setPassword(e.target.value)
    }
    const handleUsernameParamChange = (e) => {
        setUsernameParam(e.target.value)
    }

    const handleWebsiteUrlChange = (e) => {
        setWebsiteUrl(e.target.value)
    }

    const handlePasswordParamChange = (e) => {
        setPasswordParam(e.target.value)
    }
    const handlePostLoginChange = (e) => {
        setPostLogin(e.target.value)
    }
    const handleCloseSnackbar = (event, reason) => {
        setErrorMessage('')
        if (reason === 'clickaway') {
            return;
        }
        setHidden(false);
    }
    const notEmpty = (e) => {
        return e !== '';
    }
    const isValidUrl = (url) => {
        try {
            new URL(url);
        } catch (e) {
            setErrorMessage(e)
            setHidden(true)
            return false;
        }
        return true;
    }
    const startNewScan = (e) => {
        e.preventDefault();
        if (isValidUrl(website_url)) {
            if (!includeLogin) {
                authFetch("/api/new-scan", {
                    method: 'POST',
                    body: JSON.stringify({
                        start_url: website_url,
                        config: config
                    })
                }).then(res => {
                    if (res.status !== 200) {
                        res.json().then(data => {
                            setHidden(true)
                            setErrorMessage(data.error_message)
                        })
                    }
                    else {
                        setOpen(false);
                        setConfigOpen(false);
                    }

                })
            }
            else {
                if (notEmpty(username) && notEmpty(password) &&
                    notEmpty(usernameParam) && notEmpty(passwordParam) &&
                    notEmpty(postLogin)) {
                    authFetch("/api/new-scan", {
                        method: 'POST',
                        body: JSON.stringify({
                            start_url: website_url,
                            username: username,
                            password: password,
                            username_param: usernameParam,
                            password_param: passwordParam,
                            post_login: postLogin,
                            config: config
                        })
                    }).then(res => {
                        if (res.status !== 200) {
                            res.json().then(data => {
                                setHidden(true)
                                setErrorMessage(data.error_message)
                            })
                        }
                        else {
                            setOpen(false);
                            setConfigOpen(false);
                        }

                    })
                }
                else {
                    setHidden(true)
                    setErrorMessage("please fill all login details")
                }
            }
        }
        else {
            setHidden(true)
            setErrorMessage("please do not enter invalid url")
        }
    }
    return (
        <div>
            <Snackbar
                open={hidden}
                autoHideDuration={6000}
                anchorOrigin={{ vertical, horizontal }}
                onClose={handleCloseSnackbar}>
                <Alert onClose={handleCloseSnackbar} severity="error">
                    {error_message}
                </Alert>
            </Snackbar>
            <div className={classes.marginAutoContainer}>
                <div className={classes.marginAutoItem}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={handleClickOpen}
                        startIcon={<AddIcon />}
                        className={classes.button}
                    >
                        Add new Scan
      </Button>
                </div>
            </div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Scans</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Start a new Scan:
          </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="start_url"
                        label="start url:"
                        type="url"
                        fullWidth
                        required={true}
                        value={website_url}
                        onChange={handleWebsiteUrlChange}
                    />
                    <Button className={classes.button}
                        fullWidth
                        color="secondary"
                        variant="contained"
                        onClick={handleClickConfigOpen}>
                        advanced scan
                    </Button>
                    <FormControlLabel
                        fullWidth
                        control={
                            <Checkbox
                                checked={includeLogin}
                                name="checkedLogin"
                                color="primary"
                                onChange={handleChecked}
                            />
                        }
                        label="Include Login"
                    />
                    {
                        (includeLogin && (
                            <div>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="username"
                                    label="username:"
                                    type="text"
                                    fullWidth
                                    value={username}
                                    onChange={handleUsernameChange}
                                    required={true}
                                /> <TextField
                                    autoFocus
                                    margin="dense"
                                    id="username_param"
                                    label="username param:"
                                    type="text"
                                    fullWidth
                                    required={true}
                                    value={usernameParam}
                                    onChange={handleUsernameParamChange}
                                />  <TextField
                                    autoFocus
                                    required={true}
                                    margin="dense"
                                    id="password"
                                    label="password:"
                                    type="text"
                                    fullWidth
                                    value={password}
                                    onChange={handlePasswordChange}
                                />  <TextField
                                    autoFocus
                                    required={true}
                                    margin="dense"
                                    id="password_param"
                                    label="password param:"
                                    type="text"
                                    fullWidth
                                    value={passwordParam}
                                    onChange={handlePasswordParamChange}
                                />
                                <TextField
                                    autoFocus
                                    required={true}
                                    margin="dense"
                                    id="post_login"
                                    label="post login message: "
                                    type="text"
                                    fullWidth
                                    value={postLogin}
                                    onChange={handlePostLoginChange}
                                />
                            </div>
                        ))
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={startNewScan} color="primary">
                        start new scan
          </Button>
                </DialogActions>
            </Dialog>


            <Dialog open={configOpen} onClose={handleConfigClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Scans</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Start a new Scan:
          </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="start_url"
                        label="start url:"
                        type="url"
                        fullWidth
                        required={true}
                        value={website_url}
                        onChange={handleWebsiteUrlChange}
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.sql}
                                name="sql injection"
                                color="primary"
                                value={config.sql}
                                onChange={(e) => handleConfigChange(e, 'sql')}
                            />
                        }
                        label="sql injection"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.xss}
                                name="xss injection"
                                color="primary"
                                value={config.xss}
                                onChange={(e) => handleConfigChange(e, 'xss')}
                            />
                        }
                        label="xss injection"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.os_i}
                                name="os injection"
                                color="primary"
                                value={config.os_i}
                                onChange={(e) => handleConfigChange(e, 'os_i')}
                            />
                        }
                        label="os injection"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.php}
                                name="php injection"
                                color="primary"
                                value={config.php}
                                onChange={(e) => handleConfigChange(e, 'php')}
                            />
                        }
                        label="php injection"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.iframe}
                                name="iframe injection"
                                color="primary"
                                value={config.iframe}
                                onChange={(e) => handleConfigChange(e, 'iframe')}
                            />
                        }
                        label="iframe injection"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.idor}
                                name="idor attack"
                                color="primary"
                                value={config.idor}
                                onChange={(e) => handleConfigChange(e, 'idor')}
                            />
                        }
                        label="idor attack"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={config.html}
                                name="html injection"
                                color="primary"
                                value={config.html}
                                onChange={(e) => handleConfigChange(e, 'html')}
                            />
                        }
                        label="html injection"
                    />
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={includeLogin}
                                name="checkedLogin"
                                color="primary"
                                onChange={handleChecked}
                            />
                        }
                        label="Include Login"
                    />
                    {
                        (includeLogin && (
                            <div>
                                <TextField
                                    autoFocus
                                    margin="dense"
                                    id="username"
                                    label="username:"
                                    type="text"
                                    fullWidth
                                    value={username}
                                    onChange={handleUsernameChange}
                                    required={true}
                                /> <TextField
                                    autoFocus
                                    margin="dense"
                                    id="username_param"
                                    label="username param:"
                                    type="text"
                                    fullWidth
                                    required={true}
                                    value={usernameParam}
                                    onChange={handleUsernameParamChange}
                                />  <TextField
                                    autoFocus
                                    required={true}
                                    margin="dense"
                                    id="password"
                                    label="password:"
                                    type="text"
                                    fullWidth
                                    value={password}
                                    onChange={handlePasswordChange}
                                />  <TextField
                                    autoFocus
                                    required={true}
                                    margin="dense"
                                    id="password_param"
                                    label="password param:"
                                    type="text"
                                    fullWidth
                                    value={passwordParam}
                                    onChange={handlePasswordParamChange}
                                />
                                <TextField
                                    autoFocus
                                    required={true}
                                    margin="dense"
                                    id="post_login"
                                    label="post login message: "
                                    type="text"
                                    fullWidth
                                    value={postLogin}
                                    onChange={handlePostLoginChange}
                                />
                            </div>
                        ))
                    }
                </DialogContent>

                <DialogActions>
                    <Button onClick={handleConfigClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={startNewScan} color="primary">
                        Start advanced scan
          </Button>
                </DialogActions>
            </Dialog>


        </div >
    );
}
