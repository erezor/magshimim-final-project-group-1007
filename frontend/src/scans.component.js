import React, { useState, useEffect } from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useHistory } from 'react-router-dom';
import { authFetch } from './auth';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import InfoIcon from '@material-ui/icons/Info';
import NewScanDialog from './new_scan.dialog.component';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';


const scanStatus = ["STARTED",
    'LOGGING',
    'CRAWLING',
    'LOGIN FAILED',
    'ERROR',
    'SCANNING',
    'COMPLETE']

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Secure Your Site
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    marginAutoContainer: {
        width: 500,
        height: 80,
        display: 'flex',
        backgroundColor: 'gold',
    },
    marginAutoItem: {
        margin: 'auto'
    },
    alignItemsAndJustifyContent: {
        width: 500,
        height: 80,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'pink',
    },
}));


export default function Scans() {
    const history = useHistory();
    const classes = useStyles();
    const [scans, setScans] = useState([]);

    useEffect(() => {
        const FetchScans = () => {
            authFetch("/api/scans", {
                method: 'GET',
                body: JSON.stringify()
            }).then(res => res.json().then(data => setScans(data.scans)));
        }
        FetchScans();
        try {
            setInterval( () => {
                FetchScans();
            }, 5000);
        } catch (e) {
            console.log(e);
        }
    }, [])

    const onScanClick = (e, scanId) => {
        e.preventDefault();
        history.push({
            pathname: "/scan-info",
            state: {
                scan_id: scanId
            }
        })
    }


    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="absolute" color="default" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        Secure Your Site
        </Typography>
                </Toolbar>
            </AppBar>
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h4" align="center">
                        Scans
        </Typography>
                    <React.Fragment>
                        
                        <NewScanDialog />
                        <List disablePadding >
                            {scans.map(scan => (
                                <ListItem className={classes.listItem} key={scan.id}>
                                    <ListItemText primary={`scan: ${scan.id}`} secondary={`scan status: ${scanStatus[scan.status]}`} />
                                    {
                                        (scan.status === 6 &&
                                            <Typography variant="body2">
                                                <Button
                                                    variant="contained"
                                                    color="primary"
                                                    size="small"
                                                    className={classes.button}
                                                    startIcon={<InfoIcon />}
                                                    onClick={(e) => onScanClick(e, scan.id)}
                                                >
                                                    View Info
                                        </Button>
                                            </Typography>
                                        )}
                                </ListItem>
                            ))
                            }
                        </List>
                        <Button className={classes.button}
                            variant="contained"
                            size="large"
                            color="secondary"
                            startIcon={<ArrowBackIcon/>}

                            onClick={(e) => history.push('/login')} >
                            Log Out
                      </Button>
                    </React.Fragment>
                    <React.Fragment>
                    </React.Fragment>
                </Paper>
                <Copyright />
            </main>
        </React.Fragment>

    );
}