import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Alert from '@material-ui/lab/Alert';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import ReactPlayer from "react-player"
import zIndex from '@material-ui/core/styles/zIndex';
import './fonts.css'
function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Secure Your Site
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(0),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    text: {
        margin: theme.spacing(2),
        color: 'white',
        fontFamily: 'Roboto Mono',
        textAlign: 'center',
    },
    image: {
        width: '50%',
        height: '50%',
        padding: 0
    }
}));

export default function AboutUs() {
    const classes = useStyles();
    return (
        <Container component="main" maxWidth="s">
            <div className={classes.paper}>
                <video autoPlay loop muted id='video'>
                    <source src='https://i.imgur.com/6WjMaAd.mp4' type='video/mp4' />
                </video>
                <Typography variant="h1" className={classes.text}>Secure Your Site</Typography>
                <Typography variant="h3" className={classes.text}>By Erez or & Shachar Tzarfaty</Typography>
                <Typography variant="body1" className={classes.text}>Secure Your Site is the easiest to use automated tool for web app pentreation testing.
                        <br></br>Created with the Thought of being accesisable to the user. Our team created this tool.
                        <br></br> We target OWASP A1 category: Injections
                        <br></br>Quick Preview:
                </Typography>
                <ReactPlayer url="https://youtu.be/WIoYGu70ut0">
                </ReactPlayer>
                <Typography variant="h4" className={classes.text}>Our's Tech Stack:</Typography>
                <img src="https://imgur.com/l49Cvn7.jpg" className={classes.image} />
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}