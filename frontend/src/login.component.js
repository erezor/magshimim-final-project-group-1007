import React, { useState, useEffect } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import Paper from "@material-ui/core/Paper";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import { login, logout } from "./auth"
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import { Redirect, useHistory } from 'react-router-dom';
import Container from '@material-ui/core/Container';
function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      <Link color="inherit" href="https://material-ui.com/">
        Secure Your Site
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}



const useStyles = makeStyles((theme) => ({
  root: {
    height: "100vh",
  },
  image: {
    backgroundImage: "url(https://www.nist.gov/sites/default/files/styles/960_x_960_limit/public/images/2020/02/28/cyberstats-new-blogFeaturedImage-763-1.jpg?itok=hoyn1bIA)",
    backgroundRepeat: "no-repeat",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.grey[50]
        : theme.palette.grey[900],
    backgroundSize: "cover",
    backgroundPosition: "center",
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  footer: {
    padding: theme.spacing(3, 2),
    marginTop: 'auto',
    backgroundColor:theme.palette.grey[200] 
  },
}));
const ValidateEmail = (mail) => {
  if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)) {
    return (true)
  }
  return (false)
}
export default function Login() {
  const classes = useStyles();
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [hidden, setHidden] = useState(false);
  const [error_message, setErrorMessage] = useState('')
  const [vertical] = useState('top')
  const [horizontal] = useState('center')
  const passw = /^[A-Za-z]\w{7,14}$/;
  const history = useHistory();

  useEffect(() => {
    logout(); // remove previos logins
  })

  const onSubmitClick = (e) => {
    e.preventDefault()
    if (!((password.length >= 8) || (username.match(passw)))) {
      setErrorMessage('Password must contain at least 8 characters a-z, A-Z, 1-9')
      setHidden(true)
    }
    if (!ValidateEmail(username)) {
      setErrorMessage('Email is not valid')
      setHidden(true)
    }
    else {
      let opts = {
        'username': username,
        'password': password
      }
      console.log(opts)
      fetch('/api/login', {
        method: 'post',
        body: JSON.stringify(opts)
      }).then(r => r.json())
        .then(token => {
          if (token.access_token) {
            login(token)
            console.log(token)
            history.push('/dashboard');
          }
          else {
            setHidden(true)
            setErrorMessage("invalid cradientials")
          }
        })
    }
  }
  const handleClose = (event, reason) => {
    setErrorMessage('')
    if (reason === 'clickaway') {
      return;
    }

    setHidden(false);
  };
  const handleUsernameChange = (e) => {
    setUsername(e.target.value)
  }

  const handlePasswordChange = (e) => {
    setPassword(e.target.value)
  }
  return (
    <div>
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Snackbar
        open={hidden}
        autoHideDuration={6000}
        anchorOrigin={{ vertical, horizontal }}
        onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {error_message}
        </Alert>
      </Snackbar>
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Log in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              value={username}
              onChange={handleUsernameChange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              value={password}
              onChange={handlePasswordChange}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={onSubmitClick}
            >
              Log in
            </Button>
            <Grid container>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
    <footer className={classes.footer}>
        <Container maxWidth="sm" >
          <Typography variant="body1">
            <Typography variant="body2" color="textSecondary" align="center">
              <Link href="/about-us">
                About us
              </Link>
            </Typography>
            </Typography>
          <Copyright />
        </Container>
      </footer>
    </div>
  );
}
