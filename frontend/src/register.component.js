import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Alert from '@material-ui/lab/Alert';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import { useHistory } from 'react-router-dom';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Secure Your Site
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function Register() {
  const classes = useStyles();
  const history = useHistory();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [websiteUrl, setWebsiteUrl] = useState('');
  const [hidden, setHidden] = useState(false);
  const [vertical] = useState('top')
  const [horizontal] = useState('center')
  const [error_message, setErrorMessage] = useState('')

  const passw = /^[A-Za-z]\w{7,14}$/;

  const ValidateEmail = (mail) => {
    if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)) {
      return (true)
    }
    return (false)
  }
  const isValidUrl = (url) => {
    try {
      new URL(url);
    } catch (e) {
      setErrorMessage(e)
      setHidden(true)
      return false;
    }
    return true;
  }
  const onSubmitClick = (e) => {
    e.preventDefault()
    if (!((password.length >= 8) || (username.match(passw)))) {
      setErrorMessage('Password must contain at least 8 characters a-z, A-Z, 1-9')
      setHidden(true)
    }
    else if (!isValidUrl(websiteUrl)) {
      setErrorMessage('Website URL is not valid')
      setHidden(true)
    }
    else if (!ValidateEmail(username)) {
      setErrorMessage('Email is not valid')
      setHidden(true)

    }
    else {
      let opts = {
        'username': username,
        'password': password,
        'website_url': websiteUrl
      }
      fetch('/api/register', {
        method: 'post',
        body: JSON.stringify(opts)
      }).then((res) => {
        if (res.status === 422) {
          res.json().then(data => {
            setErrorMessage(data.error_message)
            setHidden(true)
          })
        }
        else {
          res.json().then(data => {
            let path = '/register-verify'
            history.push({
              pathname: path,
              state: data
            });
          })

        }
      }).catch((err) => {
        console.error(err)
      })
    }
  }
  const handleUsernameChange = (e) => {
    setUsername(e.target.value)
  }

  const handlePasswordChange = (e) => {
    setPassword(e.target.value)
  }
  const handleWebsiteUrlChange = (e) => {
    setWebsiteUrl(e.target.value)
  }
  const handleClose = (event, reason) => {
    setErrorMessage('')
    if (reason === 'clickaway') {
      return;
    }

    setHidden(false);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <Snackbar
        open={hidden}
        autoHideDuration={6000}
        anchorOrigin={{ vertical, horizontal }}
        onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          {error_message}
        </Alert>
      </Snackbar>
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={username}
                onChange={handleUsernameChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={handlePasswordChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="website url"
                label="website url"
                type="url"
                id="website_url"
                autoComplete="current-website-url"
                value={websiteUrl}
                onChange={handleWebsiteUrlChange}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={onSubmitClick}
          >
            Register
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="./" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );

}  