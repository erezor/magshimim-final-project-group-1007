import React, { createRef, useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Alert from '@material-ui/lab/Alert';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Snackbar from '@material-ui/core/Snackbar';
import SaveIcon from '@material-ui/icons/Save';
import { Redirect, useHistory, useLocation } from 'react-router-dom';
import DoneOutlineIcon from '@material-ui/icons/DoneOutline';
import CancelIcon from '@material-ui/icons/Cancel';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Secure Your Site
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: "#8bc34a",
        marginLeft: theme.spacing(2)
    },
    cancel: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: "#f44336",
        marginRight: theme.spacing(2)
    },
    typography: {
        m: 1,
        fontWeight: 600,
        textAlign: "center",
        color: theme.palette.primary.dark,
        border: "4px solid black",
    },
    button: {
        margin: theme.spacing(1),
    },
}));

export default function RegisterVerify() {
    const classes = useStyles();
    const location = useLocation();
    const [hidden, setHidden] = useState(false);
    const [vertical] = useState('top')
    const [horizontal] = useState('center')
    const [error_message, setErrorMessage] = useState('')
    const history = useHistory();
    const state = location.state;
    let dofileDownload = createRef()
    const fileName = 'sys-key.ddd'
    const [fileDownloadUrl, setFileDownloadUrl] = useState(null)
    const onCancelClick = (e) => {
        e.preventDefault();
        history.goBack();
    }
    const onSubmitClick = (e) => {
        e.preventDefault();
        let opts = {
            'website_url': state.website_url
        }
        fetch('/api/register-verify', {
            method: 'post',
            body: JSON.stringify(opts)
        }).then(res => {
            console.log(opts)
            res.json().then(data => {
                console.log(data)
                if (res.status !== 200) {
                    setErrorMessage(data.error_message)
                    setHidden(true)
                }
                else {
                    history.push('/login')
                }
            })
        })
    }
    const download = (e) => {
        e.preventDefault()
        let output = state.secret;
        const blob = new Blob([output]);
        const fileDownloadUrl = URL.createObjectURL(blob);
        setFileDownloadUrl(fileDownloadUrl)
        dofileDownload.click();
        //URL.revokeObjectURL(fileDownloadUrl);  // free up storage--no longer needed.
        //setFileDownloadUrl("")
    }
    const handleClose = (event, reason) => {
        setErrorMessage('')
        if (reason === 'clickaway') {
            return;
        }

        setHidden(false);
    };
    if (state === undefined) {
        return <Redirect to='/register'></Redirect>
    }
    return (

        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Snackbar
                open={hidden}
                autoHideDuration={6000}
                anchorOrigin={{ vertical, horizontal }}
                onClose={handleClose}>
                <Alert onClose={handleClose} severity="error">
                    {error_message}
                </Alert>
            </Snackbar>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Verify site's ownership
        </Typography>
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <Typography variant="body1"
                                className={classes.typography}>
                                <p>Instructions:</p>
                                <p>Download the file below </p>
                                <p>Do not rename it!! </p>
                                <p>Place it under the website home directory </p>
                                <p>When Done press verify website  </p>
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                className={classes.button}
                                startIcon={<SaveIcon />}
                                onClick={download}
                            >
                                Save
                            </Button>
                            <a hidden={true}
                                download={fileName}
                                href={fileDownloadUrl}
                                ref={e => dofileDownload = e}
                            >download it</a>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} justify="center">
                        <Grid item>

                        </Grid>
                        <Grid item>
                            <Button
                                variant="contained"
                                color="secondary"
                                size="large"
                                className={classes.button}
                                startIcon={<CancelIcon />}
                                onClick={onCancelClick}
                            >
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                className={classes.button}
                                startIcon={<DoneOutlineIcon />}
                                onClick={onSubmitClick}
                            >
                                Verify
                            </Button>
                        </Grid>
                    </Grid>

                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );

}  