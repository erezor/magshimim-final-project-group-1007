import React, { useState, useEffect} from 'react';
import Link from '@material-ui/core/Link';
import jsPDF from 'jspdf';
import * as htmlToImage from 'html-to-image';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useHistory, useLocation } from 'react-router-dom';
import { authFetch } from './auth';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PictureAsPdfIcon from '@material-ui/icons/PictureAsPdf';
import CanvasJSReact from './canvasjs.react';


var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Secure Your Site
      </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    appBar: {
        position: 'relative',
    },
    layout: {
        width: 'auto',
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
            width: 600,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    stepper: {
        padding: theme.spacing(3, 0, 5),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
        backgroundColor: "#ff0f43",
        color: "#fff"
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    listItem: {
        padding: theme.spacing(1, 0),
    },
    total: {
        fontWeight: 700,
    },
    title: {
        marginTop: theme.spacing(2),
    },
}));


export default function ScanInfo() {
    const location = useLocation();
    const history = useHistory();
    const classes = useStyles();
    const state = location.state;
    const [scanInfo, setScanInfo] = useState({
        id: 0,
        status: 0,
        start_time: "",
        end_time: "",
        sql_links: [],
        xss_links: [],
        os_links: [],
        php_links: [],
        idor_links: [],
        html_links: [],
        iframe_links: []
    });
    const [scanStats, setScanStats] = useState({
        id: 0,
        total_links: 0,
        sql_links: 0,
        xss_links: 0,
        os_links: 0,
        iframe_links: 0,
        html_links: 0,
        php_links: 0,
        idor_links: 0
    })

    const redirect = () => {
        history.goBack();
    };

    const FetchScan = () => {
        if (state !== undefined) {
            authFetch("/api/scan-info", {
                method: 'POST',
                body: JSON.stringify({ scan_id: state.scan_id })
            }).then(res => {
                if (res.status !== 200) redirect();
                else {
                    res.json().then(data => {
                        if (data.scan_info.end_time) {
                            setScanInfo({
                                id: data.scan_info.id,
                                status: data.scan_info.status,
                                start_time: data.scan_info.start_time,
                                end_time: data.scan_info.end_time,
                                sql_links: data.scan_info.sql_links.split(","),
                                xss_links: data.scan_info.xss_links.split(","),
                                os_links: data.scan_info.os_links.split(","),
                                php_links: data.scan_info.php_links.split(","),
                                iframe_links: data.scan_info.iframe_links.split(","),
                                idor_links: data.scan_info.idor_links.split(","),
                                html_links: data.scan_info.html_links.split(","),
                            });
                            setScanStats({
                                id: data.scan_stats.id,
                                total_links: data.scan_stats.total_links,
                                sql_links: data.scan_stats.sql_links,
                                xss_links: data.scan_stats.xss_links,
                                os_links: data.scan_stats.os_links,
                                php_links: data.scan_stats.php_links,
                                iframe_links: data.scan_stats.iframe_links,
                                html_links: data.scan_stats.html_links,
                                idor_links: data.scan_stats.idor_links,
                            })
                            console.log(scanInfo);
                            console.log(scanStats);
                        }
                        else {
                            redirect();
                        }
                    });
                }
            });
        }
    }
    useEffect(() => {
        FetchScan();
    }, [])


    const onCancelClick = (e) =>{
        e.preventDefault();
        history.push('/dashboard')
    }
    const printDocument = () => {
        htmlToImage.toPng(document.querySelector("#root"), { quality: 0.95 })
            .then(function (dataUrl) {
                var link = document.createElement('a');
                link.download = 'screenshot.jpeg';
                const pdf = new jsPDF('p', 'mm', [297, 210]);//a4 size
                const imgProps = pdf.getImageProperties(dataUrl);
                const pdfWidth = pdf.internal.pageSize.getWidth();
                const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
                pdf.addImage(dataUrl, 'PNG', 0, 0, pdfWidth, pdfHeight);
                pdf.save(`scan_${scanInfo.id}_report.pdf`); 
            });

    }
    if (state === undefined || state.scan_id === undefined) {
        redirect();
    }
    let dataPoint;
    let total;
    const optionss = {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Scan Stats:"
        },
        data: [{
            type: "funnel",
            reversed: true,
            toolTipContent: "<b>{label}</b>: {y} <b>({percentage}%)</b>",
            indexLabelPlacement: "outsided",
            indexLabel: "{label} ({percentage}%)",
            dataPoints: [
                { y: scanStats.total_links, label: "Total links" },
                { y: scanStats.sql_links, label: "SQL Injetion" },
                { y: scanStats.xss_links, label: " XSS Injection" },
                { y: scanStats.os_links, label: "OS Injection" },
                { y: scanStats.php_links, label: "PHP Injection" },
                { y: scanStats.iframe_links, label: "IFRAME Injection" },
                { y: scanStats.idor_links, label: "Insecure Data Object Refrence" },
                { y: scanStats.html_links, label: "HTML Injection" }
            ]
        }]
    }
    //calculate percentage
    dataPoint = optionss.data[0].dataPoints;
    total = dataPoint[0].y;
    for (var i = 0; i < dataPoint.length; i++) {
        if (i === 0) {
            optionss.data[0].dataPoints[i].percentage = 100;
        } else {
            optionss.data[0].dataPoints[i].percentage = ((dataPoint[i].y / total) * 100).toFixed(2);
        }
    }

    const baroptions = {
        animationEnabled: true,
        theme: "dark2",
        title: {
            text: "Vulnerabilities count"
        },
        axisX: {
            title: "Vulnerabilities",
            reversed: true,
        },
        axisY: {
            title: "Amount Of Times Detected",
            includeZero: true
        },
        data: [{
            type: "bar",
            dataPoints: [
                { y: scanStats.sql_links, label: "SQL Injetion" },
                { y: scanStats.xss_links, label: " XSS Injection" },
                { y: scanStats.os_links, label: "OS Injection" },
                { y: scanStats.php_links, label: "PHP Injection" },
                { y: scanStats.iframe_links, label: "IFRAME Injection" },
                { y: scanStats.idor_links, label: "Insecure Data Object Refrence" },
                { y: scanStats.html_links, label: "HTML Injection" }
            ]
        }]
    }
    return (
        <React.Fragment>
            <CssBaseline />
            <AppBar position="absolute" color="default" className={classes.appBar}>
                <Toolbar>
                    <Typography variant="h6" color="inherit" noWrap>
                        Secure Your Site
          </Typography>
                </Toolbar>
            </AppBar>
            <main className={classes.layout}>
                <Paper className={classes.paper}>
                    <Typography component="h1" variant="h4" align="center">
                        Scan Info
          </Typography>
                    <React.Fragment>
                        <Typography variant="h6" gutterBottom>
                            {"Scan ID: "}
                            {scanInfo.id}
                        </Typography>
                        <CanvasJSChart options={optionss} key={optionss.data[0].dataPoints.toString()}/>
                        <CanvasJSChart options={baroptions} key={baroptions.data[0].dataPoints.toString()} />
                        <List disablePadding >
                            <ListItem className={classes.listItem} key={"status"}>
                                <ListItemText primary={"status:"} secondary={"FINISHED"} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"start_time"}>
                                <ListItemText primary={"start time:"} secondary={scanInfo.start_time} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"end_time"}>
                                <ListItemText primary={"end time:"} secondary={scanInfo.end_time} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"sql_links"}>
                                <ListItemText primary={"sql links:"} secondary={scanInfo.sql_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"xss_links"}>
                                <ListItemText primary={"xss links:"} secondary={scanInfo.xss_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"os_links"}>
                                <ListItemText primary={"os links:"} secondary={scanInfo.os_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"php_links"}>
                                <ListItemText primary={"php links:"} secondary={scanInfo.php_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"iframe_links"}>
                                <ListItemText primary={"iframe links:"} secondary={scanInfo.iframe_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"html_links"}>
                                <ListItemText primary={"html links:"} secondary={scanInfo.html_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>
                            <ListItem className={classes.listItem} key={"idor_links"}>
                                <ListItemText primary={"idor links:"} secondary={scanInfo.idor_links.map(link => {
                                    return (
                                        <Link href={link} color="primary">
                                            {link}
                                            <br></br>
                                        </Link>
                                    )
                                })} />
                            </ListItem>

                        </List>
                        <Button onClick={onCancelClick} className={classes.button} 
                            startIcon={<ArrowBackIcon />}
                        >
                            Go back
                        </Button>
                        <Button onClick={printDocument} className={classes.button} startIcon={<PictureAsPdfIcon />} >
                            Generate PDF
                        </Button>
                    </React.Fragment>
                    <React.Fragment>
                    </React.Fragment>
                </Paper>
                <Copyright />
            </main>
        </React.Fragment>

    );
}