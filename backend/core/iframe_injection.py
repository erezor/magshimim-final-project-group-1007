from bs4 import BeautifulSoup as bs
from urllib.parse import urlparse


def get_all_frames(url, session):
    """Given a `url`, it returns all forms from the HTML content"""
    soup = bs(session.get(url).content, "html.parser")
    return soup.find_all("iframe")


def get_url_params(url, session):
    """
    This function extracts all possible useful information about an HTML `frame`
    """
    params = {}
    params_str = urlparse(session.get(url).url).query

    try:
        params = dict(x.split('=') for x in params_str.split('&'))
    except ValueError as e:
        pass  # no params were found
    finally:
        return params


def submit_frame(params, url, value, session):
    for param in params.keys():
        # replace all text and search values with `value`
        params[param] = value

    return session.get(url, params=params)


def scan(url, session):
    """
    a function that scans whether or not a URL is vulnerable to iframe injection attack
    :param url: url to scan
    :return: boolean value
    true = vulnerable
    false = safe
    """
    # get all the forms from the URL
    frames = get_all_frames(url, session)

    frame_script = "></iframe><script>alert('hi');</script>"
    # returning value
    is_vulnerable = []
    # iterate over all forms
    params = get_url_params(url, session)
    for frame in frames:
        content = submit_frame(params, url, frame_script, session).content.decode()
        if frame_script in content:
            is_vulnerable.append(True)
            # won't break because we want to print other available vulnerable forms
        else:
            is_vulnerable.append(False)
    return True in is_vulnerable
