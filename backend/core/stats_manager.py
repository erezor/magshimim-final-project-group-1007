class StatsManager:
    def __init__(self,stats):
        self.stats = stats

    def get_sql_stats(self):
        stats = (self.stats.sql_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_xss_stats(self):
        stats = (self.stats.xss_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_idor_stats(self):
        stats = (self.stats.idor_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_php_stats(self):
        stats = (self.stats.php_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_html_stats(self):
        stats = (self.stats.html_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_os_stats(self):
        stats = (self.stats.os_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_iframe_stats(self):
        stats = (self.stats.iframe_links / self.stats.total_links) * 100
        return "{:.2f}".format(stats)

    def get_stats(self):
        return {
            "sql_stats": self.get_sql_stats(),
            "xss_stats": self.get_xss_stats(),
            "html_stats": self.get_html_stats(),
            "idor_stats": self.get_idor_stats(),
            "iframe_stats": self.get_iframe_stats(),
            "php_stats": self.get_php_stats(),
            "os_stats": self.get_os_stats()
        }
