import requests
# import re # uncomment this for DVWA
from urllib.parse import urljoin
import core.helper


def is_vulnerable(response):
    """
    function to check whether or not the scan succeededd
    :param response: the HTML page returned from the website
    :return: boolean value
    true = vulnerable
    false = safe
    """
    # if you find one of these errors, return True
    if b"<h1>erezor</h1>" in response.content:
        return True
    # no error detected
    return False


def scan(url, session):
    """
    a function that scans whether or not a URL is vulnerable to HTML injection attack
    :param url: url to scan
    :param session: requests session
    :return: boolean value
    true = vulnerable
    false = safe
    """
    # test on URL
    for c in "\"'":
        # add quote/double quote character to the URL
        new_url = f"{url}{c}"
        # make the HTTP request
        res = session.get(new_url)
        if is_vulnerable(res):
            # SQL Injection detected on the URL itself,
            # no need to preceed for extracting forms and submitting them
            return True
    # test on HTML forms
    vulforms = []
    forms = core.helper.helper.get_all_forms(url, session)
    for form in forms:
        form_details = core.helper.helper.get_form_details(form)
        for c in "\"'":
            # the data body we want to submit
            data = {}
            for input_tag in form_details["inputs"]:
                if input_tag["value"] or input_tag["type"] == "hidden":
                    # any input form that has some value or hidden,
                    # just use it in the form body
                    try:
                        data[input_tag["name"]] = input_tag["value"] + c
                    except:
                        pass
                elif input_tag["type"] != "submit":
                    # all others except submit, use some junk data with special character
                    data[input_tag["name"]] = f"{c}<h1>erezor</h1>"
            # join the url with the action (form request URL)
            url = urljoin(url, form_details["action"])
            if form_details["method"] == "post":
                res = session.post(url, data=data)
            elif form_details["method"] == "get":
                res = session.get(url, params=data)
            # test whether the resulting page is vulnerable
            if is_vulnerable(res):
                vulforms.append(True)
                break
    return True in vulforms
