from urllib.parse import urlparse, parse_qsl, urlsplit
import re

words = ['id', 'user', 'account', 'number', 'order', 'no', 'doc', 'key', 'email', 'group', 'profile', 'edit', 'report',
         'pw', 'uid']


def scan(url, session):
    """
        a function that scans whether or not a URL is vulnerable to Insecure Direct Object Reference attack
        :param url: url to scan
        :return: boolean value
        true = vulnerable
        false = safe
        """
    o = urlparse(url)
    params = list(dict(parse_qsl(urlsplit(url).query)).keys())
    params = [element.lower() for element in params]

    result = []

    for x in words:
        y = re.compile(".*" + x)
        if len(list(filter(y.match, params))) > 0:
            result.append(x)

    if True in result:
        return True
    return False
