import core.helper


def scan(url, session):
    """
        a function that scans whether or not a URL is vulnerable to OS injection attack
        :param url: url to scan
        :return: boolean value
        true = vulnerable
        false = safe
        """
    # get all the forms from the URL
    forms = core.helper.helper.get_all_forms(url, session)
    os_scripts = ["input | sleep 5", "input & sleep 5", "input | sleep 5", "input ; sleep 5"]
    is_vulnerable = []
    for os_script in os_scripts:
        for form in forms:
            form_details = core.helper.helper.get_form_details(form)
            is_vulnerable.append(
                core.helper.helper.submit_form(form_details, url, os_script, session).elapsed.total_seconds() >= 5)
            if True in is_vulnerable:
                return True
    return False
