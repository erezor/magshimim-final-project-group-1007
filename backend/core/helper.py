import requests
from bs4 import BeautifulSoup as bs
from urllib.parse import urljoin

# make requests get and post
# submit form
# get all forms
# get form details
"""
the helper is made to prevent repeating code 
"""
global helper


class Helper:
    def __init__(self):
        self.s = requests.Session()
        self.s.headers[
            "User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Safari/537.36"

    def set_cookies(self, cookies):
        """
        function to set cookies in the Helper object
        :param cookies: cookies to set
        :return: none
        """
        for key in cookies.keys():
            self.s.cookies.set_cookie(requests.cookies.create_cookie(key, cookies[key]))

    @staticmethod
    def get_all_forms(url, session):
        """Given a `url`, it returns all forms from the HTML content"""
        soup = bs(session.get(url).content, "html.parser")
        return soup.find_all("form")

    @staticmethod
    def get_form_details(form):
        """
        This function extracts all possible useful information about an HTML `form`
        """
        details = {}
        # get the form action (target url)
        try:
            action = form.attrs.get("action").lower()
        except:
            action = None
        # get the form method (POST, GET, etc.)
        method = form.attrs.get("method", "get").lower()
        # get all the input details such as type and name
        inputs = []
        for input_tag in form.find_all("input"):
            input_type = input_tag.attrs.get("type", "text")
            input_name = input_tag.attrs.get("name")
            input_value = input_tag.attrs.get("value", "")
            inputs.append({"type": input_type, "name": input_name, "value": input_value})
        # put everything to the resulting dictionary
        details["action"] = action
        details["method"] = method
        details["inputs"] = inputs
        return details

    def submit_form(self, form_details, url, value,session):
        """
        Submits a form given in `form_details`
        Params:
            form_details (list): a dictionary that contain form information
            url (str): the original URL that contain that form
            value (str): this will be replaced to all text and search inputs
        Returns the HTTP Response after form submission
        """
        # construct the full URL (if the url provided in action is relative)
        target_url = urljoin(url, form_details["action"])
        # get the inputs
        inputs = form_details["inputs"]
        data = {}
        for input in inputs:
            # replace all text and search values with `value`
            if input["type"] == "text" or input["type"] == "search":
                input["value"] = value
            input_name = input.get("name")
            input_value = input.get("value")
            if input_name and input_value:
                # if input name and value are not None,
                # then add them to the data of form submission
                data[input_name] = input_value

        if form_details["method"] == "post":
            return self.s.post(target_url, data=data)
        else:
            # GET request
            return self.s.get(target_url, params=data)


# each injection script should not have a name start script name should be scan and get only url param

helper = Helper()
