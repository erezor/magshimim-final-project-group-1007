import requests

from bs4 import BeautifulSoup as bs
from urllib.parse import urljoin
import core.helper


def scan(url, session):
    """
    a function that scans whether or not a URL is vulnerable to XSS injection attack
    :param url: url to scan
    :return: boolean value
    true = vulnerable
    false = safe
    """
    # get all the forms from the URL
    forms = core.helper.helper.get_all_forms(url, session)
    js_script = "<Script>alert('hi')</script>"
    # returning value
    is_vulnerable = []
    # iterate over all forms
    for form in forms:
        form_details = core.helper.helper.get_form_details(form)
        content = core.helper.helper.submit_form(form_details, url, js_script, session).content.decode()
        if js_script in content:
            is_vulnerable.append(True)
            # won't break because we want to print other available vulnerable forms
        else:
            is_vulnerable.append(False)
    return True in is_vulnerable
