from config_app import celery, db
from models.ScanModel import Scan
from models.StatsModel import Stats
from core.helper import helper
import core.html_injection
import core.idor_attack
import core.iframe_injection
import core.os_injection
import core.php_Injection
import core.sql_injection
import core.xss_injection
from core.crawler import Scraper
from datetime import datetime
from concurrent.futures import ThreadPoolExecutor, wait
import threading


# bwapp command example
# python3 scan_handler.py --login_url http://localhost/login.php --username_
# param login --password_param password --username bee --password bug --post_login portal

class VulnerablityLinks:
    sql_links = []
    xss_links = []
    idor_links = []
    iframe = []
    os_i_links = []
    php_links = []
    html_links = []


STATUS = {
    'START': 0,
    'LOGGING': 1,
    'CRAWL': 2,
    'LOGIN_FAILED': 3,
    'ERROR': 4,
    'SCAN': 5,
    'COMPLETE': 6
}

thread_local = threading.local()

config = None


def get_session():
    if not hasattr(thread_local, "session"):
        thread_local.session = helper.s
    return thread_local.session


def get_config() -> dict:
    if not hasattr(thread_local, "config"):
        thread_local.config = config
    return thread_local.config


def update_scan_db(scan, status, stats=None, links=None, update_res=False):
    scan.status = status
    if update_res:
        scan.end_time = datetime.now()
        scan.sql_links = ','.join(VulnerablityLinks.sql_links)
        scan.xss_links = ','.join(VulnerablityLinks.xss_links)
        scan.idor_links = ','.join(VulnerablityLinks.idor_links)
        scan.iframe_links = ','.join(VulnerablityLinks.iframe)
        scan.os_links = ','.join(VulnerablityLinks.os_i_links)
        scan.php_links = ','.join(VulnerablityLinks.php_links)
        scan.html_links = ','.join(VulnerablityLinks.html_links)
    if stats is not None and links is not None:
        stats.total_links = len(links)
        stats.sql_links = len(VulnerablityLinks.sql_links)
        stats.xss_links = len(VulnerablityLinks.xss_links)
        stats.idor_links = len(VulnerablityLinks.idor_links)
        stats.iframe_links = len(VulnerablityLinks.iframe)
        stats.os_links = len(VulnerablityLinks.os_i_links)
        stats.php_links = len(VulnerablityLinks.php_links)
        stats.html_links = len(VulnerablityLinks.html_links)
    db.session.flush()
    db.session.commit()


def scan_all(url):
    try:
        print("scanning for vulnerbilaties at ", url)
        session = get_session()
        conf = get_config()
        if conf['sql']:
            if core.sql_injection.scan(url, session):
                VulnerablityLinks.sql_links.append(url)
        if conf['xss']:
            if core.xss_injection.scan(url, session):
                VulnerablityLinks.xss_links.append(url)
        if conf['idor']:
            if core.idor_attack.scan(url, session):
                VulnerablityLinks.idor_links.append(url)
        if conf['iframe']:
            if core.iframe_injection.scan(url, session):
                VulnerablityLinks.iframe.append(url)
        if conf['os_i']:
            if core.os_injection.scan(url, session):
                VulnerablityLinks.os_i_links.append(url)
        if conf['php']:
            if core.php_Injection.scan(url, session):
                VulnerablityLinks.php_links.append(url)
        if conf['html']:
            if core.html_injection.scan(url, session):
                VulnerablityLinks.html_links.append(url)
    except Exception as e:
        print("Error at ", url)
        print(e)


@celery.task()
def start_scan(start_url, username, password, username_param, password_param, post_login, scan_id, conf):
    try:
        global config
        config = conf
        scan = Scan.query.filter_by(id=scan_id).first()
        stats = Stats.query.filter_by(id=scan_id).first() or None
        links = None
        scraper = Scraper()
        helper.set_cookies({'test': 'test'})
        flag = True
        if username is not None:
            update_scan_db(scan, STATUS['LOGGING'])
            if not scraper.login(start_url, username_param, password_param, username, password,
                                 post_login):
                update_scan_db(scan, STATUS['LOGIN_FAILED'])
                flag = False
        if flag:
            print('Crawling')
            update_scan_db(scan, STATUS['CRAWL'])
            links = scraper.crawl_website()
            links = list(set(links))
            update_scan_db(scan, STATUS['SCAN'])
            helper.set_cookies(scraper.get_cookies())
            print('scanning')
            with ThreadPoolExecutor(max_workers=5) as executor:
                results = [executor.submit(scan_all, link) for link in links]
                wait(results)
            print('complete')
            update_scan_db(scan, STATUS['COMPLETE'], stats, links, True)
    except Exception as e:
        print(e)
        update_scan_db(scan, STATUS['ERROR'], stats, links)
