class Config:
    def __init__(self, config_json):
        self.sql = config_json['sql']
        self.xss = config_json['xss']
        self.os_i = config_json['os_i']
        self.php = config_json['php']
        self.iframe = config_json['iframe']
        self.html = config_json['html']
        self.idor = config_json['idor']