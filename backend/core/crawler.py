from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

from requests_html import HTMLSession
from urllib.parse import urlparse, urljoin
from bs4 import BeautifulSoup
import requests
from bs4 import BeautifulSoup as bs
import time
import os


class Scraper:
    """Scraper class is meant to crawl through a given URL and get all the possible URLs from it.
    based on Selenium API.
    """

    def __init__(self):
        """initialization function for the Scraper objecct"""
        self.options = Options()
        self.options.binary = os.getenv("FIREFOX_BIN")
        self.options.headless = True
        self.options.add_argument("--disable-dev-shm-usage")
        self.options.add_argument("--no-sandbox")
        self.firefox_profile = webdriver.FirefoxProfile()
        self.firefox_profile.set_preference('browser.download.folderList', 2)
        self.firefox_profile.set_preference('browser.download.manager.showWhenStarting', False)
        self.firefox_profile.set_preference('browser.download.dir', os.getcwd())
        self.firefox_profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv')
        self.driver = webdriver.Firefox(executable_path=os.getenv("GECKODRIVER_PATH"), options=self.options,
                                        firefox_profile=self.firefox_profile)
        self.cookies = {}
        self.timeout = 13
        self.internal_urls = []
        self.external_urls = []
        self.total_urls_visited = 0
        self.max_urls = 30
        self.urls = []
        # domain name of the URL without the protocol
        self.domain_name = ''
        # initialize an HTTP session
        self.session = HTMLSession()

    def login(self, login_url, username_param, password_param, username, password, login_msg):
        self.driver.get(login_url)
        self.domain_name = urlparse(self.driver.current_url).netloc

        """
        function to automatically log into a given URL using the username and password from 'username_param' and 'password_param' parameters 
        :return NONE
        """
        try:
            # wait for page to load
            element_present = EC.presence_of_element_located((By.ID, username_param))
            WebDriverWait(self.driver, self.timeout).until(element_present)
            self.driver.find_element_by_id(username_param).send_keys(username)
            self.driver.find_element_by_id(password_param).send_keys(password)
            button = self.driver.find_element_by_css_selector("button[type='submit']")
            self.driver.execute_script("arguments[0].click();", button)

            for cookie in self.driver.get_cookies():
                self.session.cookies.set_cookie(requests.cookies.create_cookie(cookie['name'], cookie['value']))
            time.sleep(2)
            return login_msg in self.driver.page_source

        except TimeoutException:
            print("Timed out waiting for page to load")

    def get_cookies(self):
        """
        Function to get the cookies from the website
        :return: a dict of the cookies
        """
        for cookie in self.driver.get_cookies():
            self.cookies[cookie['name']] = cookie['value']
        return self.cookies

    @staticmethod
    def is_valid(url):
        """
        Checks whether `url` is a valid URL.
        :return bool object
        """
        parsed = urlparse(url)
        return bool(parsed.netloc) and bool(parsed.scheme)

    def get_all_website_links(self, url):
        """
        Returns all URLs that is found on `url` in which it belongs to the same website
        :return list of all URLs
        """
        print("currently crawling " + self.driver.current_url)
        # all URLs of `url`
        self.domain_name = urlparse(self.driver.current_url).netloc
        # make HTTP request & retrieve response
        response = self.session.get(url)
        # execute Javascript
        soup = BeautifulSoup(response.text, "html.parser")
        for a_tag in soup.findAll("a"):
            href = a_tag.attrs.get("href")
            if href == "" or href is None:
                # href empty tag
                continue
            # join the URL if it's relative (not absolute link)
            href = urljoin(url, href)
            parsed_href = urlparse(href)
            # remove URL GET parameters, URL fragments, etc.
            href = parsed_href.scheme + "://" + parsed_href.netloc + parsed_href.path
            if not self.is_valid(href):
                # not a valid URL
                continue
            if href in self.internal_urls:
                # already in the set
                continue
            if self.domain_name not in href:
                # external link
                if href not in self.external_urls:
                    self.external_urls.append(href)
                continue
            self.urls.append(href)
            self.internal_urls.append(href)
        return self.urls

    def get_page_form_links(self):
        """
        saves all links that is found in a specific form in self.internal_urls
        :return: none
        """
        print("currently crawling form links on " + self.driver.current_url)
        if self.driver.current_url not in self.internal_urls:
            try:
                back_url = self.driver.current_url
                select = self.driver.find_element_by_css_selector('select')
                options_range = len(select.find_elements_by_css_selector('option'))
                self.driver.set_page_load_timeout(10)
                for option in range(0, options_range):

                    try:
                        self.driver.find_element_by_css_selector('select').find_elements_by_css_selector('option')[
                            option].click()
                        self.driver.find_element_by_css_selector("button[type='submit']").send_keys(Keys.ENTER)
                        start = time.time()
                        loading = True
                        while time.time() - start <= 5 and loading:
                            if self.driver.current_url != back_url:
                                loading = False
                                print("form link found " + self.driver.current_url)
                        if self.domain_name in self.driver.current_url:
                            self.internal_urls.append(self.driver.current_url)
                        if self.driver.current_url != back_url:
                            self.driver.get(back_url)
                    except Exception as e:
                        print(e)
                        self.driver.get(back_url)

            except Exception as e:
                print(e)

    def crawl_page(self, url):
        """
        function to crawl a specific url and not the whole page
        :param url: the url given to crawl

        """
        self.total_urls_visited += 1
        links = self.get_all_website_links(url)
        soup = bs(self.session.get(url).content, "html.parser")
        if soup.find("form") is not None:
            self.get_page_form_links()
        for link in links:
            if self.total_urls_visited > self.max_urls:
                break
            self.crawl_page(link)

    def crawl_website(self):
        """
        the main function of the Scraper class, meant to crawl the whole website and get all possiblr links out of it
        :return: NONE
        """
        self.crawl_page(self.driver.current_url)
        self.internal_urls = list(dict.fromkeys(self.internal_urls))
        return self.internal_urls

    def __del__(self):
        try:
            self.driver.close()
        except:
            pass


if __name__ == '__main__':
    s = Scraper()
