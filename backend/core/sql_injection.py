import re
from urllib.parse import urljoin
import core.helper


def is_vulnerable(response):
    """
        function to check whether or not the scan succeededd
        :param response: the HTML page returned from the website
        :return: boolean value
        true = vulnerable
        false = safe
        """
    errors = {
        "MySQL": (r"SQL syntax.*MySQL", r"Warning.*mysql_.*", r"MySQL Query fail.*", r"SQL syntax.*MariaDB server"),
        "PostgreSQL": (r"PostgreSQL.*ERROR", r"Warning.*\Wpg_.*", r"Warning.*PostgreSQL"),
        "Microsoft SQL Server": (
        r"OLE DB.* SQL Server", r"(\W|\A)SQL Server.*Driver", r"Warning.*odbc_.*", r"Warning.*mssql_",
        r"Msg \d+, Level \d+, State \d+", r"Unclosed quotation mark after the character string",
        r"Microsoft OLE DB Provider for ODBC Drivers"),
        "Microsoft Access": (r"Microsoft Access Driver", r"Access Database Engine", r"Microsoft JET Database Engine",
                             r".*Syntax error.*query expression"),
        "Oracle": (
        r"\bORA-[0-9][0-9][0-9][0-9]", r"Oracle error", r"Warning.*oci_.*", "Microsoft OLE DB Provider for Oracle"),
        "IBM DB2": (r"CLI Driver.*DB2", r"DB2 SQL error"),
        "SQLite": (r"SQLite/JDBCDriver", r"System.Data.SQLite.SQLiteException"),
        "Informix": (r"Warning.*ibase_.*", r"com.informix.jdbc"),
        "Sybase": (r"Warning.*sybase.*", r"Sybase message")
    }
    for db, errors in errors.items():
        for error in errors:
            if re.compile(error).search(response.content.decode()):
                return True
    return False


def scan(url,session):
    """
        a function that scans whether or not a URL is vulnerable to SQL injection attack
        :param url: url to scan
        :return: boolean value
        true = vulnerable
        false = safe
        """
    for c in "\"'":
        # add quote/double quote character to the URL
        new_url = f"{url}{c}"
        # make the HTTP request
        res = session.get(new_url)
        if is_vulnerable(res):
            # SQL Injection detected on the URL itself,
            # no need to preceed for extracting forms and submitting them
            return
    # test on HTML forms
    vulforms = []
    forms = core.helper.helper.get_all_forms(url,session)
    for form in forms:
        form_details = core.helper.helper.get_form_details(form)
        for c in "\"'":
            # the data body we want to submit
            data = {}
            for input_tag in form_details["inputs"]:
                if input_tag["value"] or input_tag["type"] == "hidden":
                    # any input form that has some value or hidden,
                    # just use it in the form body
                    try:
                        data[input_tag["name"]] = input_tag["value"] + c
                    except:
                        pass
                elif input_tag["type"] != "submit":
                    # all others except submit, use some junk data with special character
                    data[input_tag["name"]] = f"test{c}"
            # join the url with the action (form request URL)
            url = urljoin(url, form_details["action"])
            if form_details["method"] == "post":
                res = session.post(url, data=data)
            elif form_details["method"] == "get":
                res = session.get(url, params=data)
            # test whether the resulting page is vulnerable
            if is_vulnerable(res):
                vulforms.append(True)
                break
    return True in vulforms
