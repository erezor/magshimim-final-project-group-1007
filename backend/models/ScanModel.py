from config_app import db


class Scan(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)
    status = db.Column(db.Integer)
    sql_links = db.Column(db.Text)
    xss_links = db.Column(db.Text)
    os_links = db.Column(db.Text)
    iframe_links = db.Column(db.Text)
    html_links = db.Column(db.Text)
    php_links = db.Column(db.Text)
    idor_links = db.Column(db.Text)

    @classmethod
    def identify(cls, id):
        return cls.query.get(id)

    @property
    def identity(self):
        return self.id

    @property
    def to_json(self):
        if self.status != 6:
            return {'id': self.id, 'status': self.status}
        return {
            'id': self.id,
            'start_time': self.start_time.strftime("%m/%d/%Y, %H:%M:%S"),
            'end_time': self.end_time.strftime("%m/%d/%Y, %H:%M:%S"),
            'status': self.status,
            'sql_links': self.sql_links,
            'xss_links': self.xss_links,
            'os_links': self.os_links,
            'iframe_links': self.iframe_links,
            'html_links': self.html_links,
            'php_links': self.php_links,
            'idor_links': self.idor_links
        }
