from config_app import db


class Stats(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    total_links = db.Column(db.Integer)
    sql_links = db.Column(db.Integer)
    xss_links = db.Column(db.Integer)
    os_links = db.Column(db.Integer)
    iframe_links = db.Column(db.Integer)
    html_links = db.Column(db.Integer)
    php_links = db.Column(db.Integer)
    idor_links = db.Column(db.Integer)

    @classmethod
    def identify(cls, id):
        return cls.query.get(id)

    @property
    def identity(self):
        return self.id

    @property
    def to_json(self):
        return {
            'id': self.id,
            'total_links': self.total_links,
            'sql_links': self.sql_links,
            'xss_links': self.xss_links,
            'os_links': self.os_links,
            'iframe_links': self.iframe_links,
            'html_links': self.html_links,
            'php_links': self.php_links,
            'idor_links': self.idor_links
        }
