import flask_praetorian
from flask import Blueprint, request
from urllib.parse import urlparse
from core import scan_handler
from core.config import Config
from models.ScanModel import Scan
from models.StatsModel import Stats
from config_app import db
from datetime import datetime

new_scan_blueprint = Blueprint('new_scan', __name__, )


@new_scan_blueprint.route('/api/new-scan', methods=['POST'])
@flask_praetorian.auth_required
def new_scan():
    """
    new scan route
    """
    req = request.get_json(force=True)
    start_url = req.get('start_url', None)
    username = req.get('username', None)
    password = req.get('password', None)
    username_param = req.get('username_param', None)
    password_param = req.get('password_param', None)
    post_login = req.get('post_login', None)
    config_json = req.get('config', None)
    if urlparse(start_url).netloc != \
            urlparse(flask_praetorian.current_user().website_url).netloc:
        return {'error_message': 'invalid url start point'}, 400

    scan = Scan(
        user_id=flask_praetorian.current_user_id(),
        start_time=datetime.now(),
        end_time=None,
        status=0,
        sql_links="",
        xss_links="",
        os_links="",
        iframe_links="",
        html_links="",
        php_links="",
        idor_links=""
    )
    db.session.add(scan)
    db.session.flush()

    db.session.commit()

    stats = Stats(id=scan.id,
                  total_links=0,
                  sql_links=0,
                  xss_links=0,
                  os_links=0,
                  iframe_links=0,
                  html_links=0,
                  php_links=0,
                  idor_links=0,
                  )
    db.session.add(stats)
    db.session.flush()

    db.session.commit()

    config = Config(config_json)
    task = scan_handler.start_scan.delay(start_url, username, password,
                                         username_param, password_param, post_login, scan.id, config_json)

    return {'message': 'scan started succesfuly'}, 200
