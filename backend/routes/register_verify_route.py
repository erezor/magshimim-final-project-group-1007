import requests
from config_app import db
from models.UserModel import User
from flask import Blueprint, request

register_verify_blueprint = Blueprint('register_verify_blueprint', __name__, )


@register_verify_blueprint.route('/api/register-verify', methods=['POST'])
def verify_register():
    req = request.get_json(force=True)
    website_url = req.get('website_url', None)
    ret = {}
    user = User.query.filter_by(website_url=website_url).first()
    isVerified = False
    if user is not None:
        try:
            con = requests.get(
                f"{website_url}/sys-key.ddd").content.decode().strip()
            isVerified = con == user.website_key
        except requests.exceptions.ConnectionError as e:
            return {"error_message": "Not avialible to get to the url"}, 400
        finally:
            user.is_active = isVerified
            db.session.commit()
            if not isVerified:
                return {"error_message": "Invalid Content in website"}, 400
            else:
                return {"status": "ok"}, 200
    else:
        ret = {"error_message": "No such user with the provided website_url"}, 400
    return ret
