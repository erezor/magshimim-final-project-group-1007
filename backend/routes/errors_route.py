from flask import Blueprint

errors_blueprint = Blueprint('errors_blueprint', __name__, )


@errors_blueprint.errorhandler(404)
def error_404(e):
    return 'Invalid url'
