import flask_praetorian
from flask import Blueprint, request, jsonify
from models.ScanModel import Scan

scans_dashboard_blueprint = Blueprint('scans_dashboard', __name__, )


@scans_dashboard_blueprint.route('/api/scans', methods=['GET'])
@flask_praetorian.auth_required
def get_scans():
    """
    scan dashboard route route
    """
    user_id = flask_praetorian.current_user().id
    scans = Scan.query.filter_by(user_id=user_id).all()
    scans_json = [scan.to_json for scan in scans]
    return jsonify(scans=scans_json), 200
