import flask_praetorian
from flask import Blueprint, request, jsonify
from models.ScanModel import Scan
from models.StatsModel import Stats
from core.stats_manager import StatsManager

scan_info_blueprint = Blueprint('scan_info', __name__, )


@scan_info_blueprint.route('/api/scan-info', methods=['POST'])
@flask_praetorian.auth_required
def get_scan_info():
    """
    get all the scan info
    """
    req = request.get_json(force=True)
    scan_id = req.get('scan_id', None)
    scan_info = Scan.query.filter_by(id=scan_id).first()
    scan_stats = Stats.query.filter_by(id=scan_id).first()
    if scan_info is None:
        return {'message': 'No such id'}, 400
    if scan_info.user_id != flask_praetorian.current_user().id:
        return {'message': 'No such id'}, 400
    stats = StatsManager(scan_stats)
    return jsonify(scan_info=scan_info.to_json, scan_stats=scan_stats.to_json), 200
