import secrets
from config_app import guard, db
from models.UserModel import User
from models.ScanModel import Scan
from flask import request, Blueprint
from urllib.parse import urlparse
register_blueprint = Blueprint('register_blueprint', __name__, )


@register_blueprint.route('/api/register', methods=['POST'])
def register():
    req = request.get_json(force=True)
    username = req.get('username', None)
    password = req.get('password', None)
    website_url = req.get('website_url', None)
    generated_key = secrets.token_urlsafe(16)
    website_url = urlparse(website_url).scheme + "://" + urlparse(website_url).netloc
    ret = {}
    if not bool(db.session.query(User).filter_by(username=username).first())\
            and not bool(db.session.query(User).filter_by(website_url=website_url).first()):
        db.session.add(User(
            username=username,
            password=guard.hash_password(password),
            website_url=website_url,
            roles='user',
            is_active=False,
            website_key=generated_key
        ))
        db.session.commit()

        return {'secret': generated_key,
                'website_url': website_url}, 200
    else:
        return {"error_message": "Username or Website url is already taken"}, 422
