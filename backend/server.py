import os
from flask import Flask, render_template, send_from_directory, request, jsonify
from config_app import config_app, guard, make_celery, db
from models.UserModel import User
from models.ScanModel import Scan
from models.StatsModel import Stats
from datetime import datetime
from flask_cors import CORS

app = Flask(__name__, static_folder="./build", static_url_path="", template_folder="./build")
config_app(app)

#  celery -A server.celery worker --loglevel=INFO --pidfile=''
# Initialize the flask-praetorian instance for the app
guard.init_app(app, User)

# Initialize a local database for the example
db.init_app(app)

# Initializes CORS so that the api_tool can talk to the example app
global celery
celery = make_celery(app)

with app.app_context():
    db.create_all()
    print("Created all")

cors = CORS(app, resources={r"/api/*": {"origins": "*"}})


# static route
@app.route("/")
def serve():
    return send_from_directory(app.static_folder, "index.html")


@app.route("/<path:path>")
def static_proxy(path):
    file_name = path.split("/")[-1]
    dir_name = os.path.join(app.static_folder, "/".join(path.split("/")[:-1]))
    return send_from_directory(dir_name, file_name)


@app.errorhandler(404)
def handle_404(e):
    if request.path.startswith("/api/"):
        return jsonify(message="Resource not found"), 404
    return send_from_directory(app.static_folder, "index.html")


# test route
@app.route('/api/')
def home():
    return {"Hello": "World"}, 200


from routes.errors_route import errors_blueprint
from routes.login_route import login_blueprint
from routes.protected_route import protected_blueprint
from routes.refresh_token_route import refresh_token_blueprint
from routes.register_route import register_blueprint
from routes.register_verify_route import register_verify_blueprint
from routes.dashboard_route import scans_dashboard_blueprint
from routes.new_scan_route import new_scan_blueprint
from routes.scan_info_route import scan_info_blueprint

# set routes
app.register_blueprint(errors_blueprint)
app.register_blueprint(login_blueprint)
app.register_blueprint(protected_blueprint)
app.register_blueprint(refresh_token_blueprint)
app.register_blueprint(register_blueprint)
app.register_blueprint(register_verify_blueprint)
app.register_blueprint(new_scan_blueprint)
app.register_blueprint(scan_info_blueprint)
app.register_blueprint(scans_dashboard_blueprint)

with app.app_context():
    try:
        if db.session.query(User).filter_by(username='erez180103@gmail.com').count() < 1:
            db.session.add(User(
                username='erez180103@gmail.com',
                password=guard.hash_password('12345678'),
                roles='admin',
                is_active=True,
                website_url="http://192.168.1.28",
                website_key='top_secret'
            ))
            db.session.commit()
            db.session.add(Scan(
                user_id=db.session.query(User).filter_by(
                    username='erez').first().id,
                start_time=datetime.now(),
                end_time=datetime.now(),
                status=0,
                sql_links="e",
                xss_links="e",
                os_links="e",
                iframe_links="e",
                html_links="e",
                php_links="e",
                idor_links="e"
            ))
            db.session.commit()
    except:
        pass
    if __name__ == '__main__':
        app.run(por)
