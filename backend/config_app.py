import flask_praetorian
import flask_sqlalchemy
from celery import Celery
import os

POSTGRESQL_URI = "postgresql://admin:admin@db:5432/sys_db"


def config_app(app):
    app.config['DEBUG'] = True
    app.config['SECRET_KEY'] = b'sI$ez`\xaf\x93,]\xc1\x03k\x04G\xb4'
    app.config['JWT_ACCESS_LIFESPAN'] = {'hours': 24}
    app.config['JWT_REFRESH_LIFESPAN'] = {'days': 30}
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URL", POSTGRESQL_URI)
    app.config['CELERY_BROKER_URL'] = os.getenv(
        'REDIS_URL', 'redis://localhost:6379')
    app.config['CELERY_RESULT_BACKEND'] = os.getenv(
        'REDIS_URL', 'redis://localhost:6379')
    # app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379'
    # app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379'


celery = None


def make_celery(app):
    global celery
    celery = Celery(
        app.import_name,
        backend=app.config['CELERY_RESULT_BACKEND'],
        broker=app.config['CELERY_BROKER_URL']
    )
    celery.conf.update(app.config)

    class ContextTask(celery.Task):
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return self.run(*args, **kwargs)

    celery.Task = ContextTask
    return celery


guard = flask_praetorian.Praetorian()

db = flask_sqlalchemy.SQLAlchemy()
